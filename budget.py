import datetime

class Record:
    startdate = ''
    no_of_entries = 0
    history = []


    def __init__(self,start,entries):
        self.startdate=start
        
    def addEntry(self):
        amount = input("Enter an amount: ")
        
        #Need a check on amount here
        
        raw_date = raw_input("Please enter a date (DD/MM/YY): ")
        
        #Need a check on date here
        
        date = self.parseDate(raw_date)
        
        self.history.append(Entries(amount,date,self.no_of_entries+1))
        
        self.no_of_entries += 1
        
        return
        
    def parseDate(self,d):
        day = d[0]+d[1]
        month = d[3]+d[4]
        year = d[6]+d[7]
        
        #return [day,month,year]
        #Does this work?
        return Date(day,month,year)

        
    def display(self):
        for i in self.history:
            print i.display_entry()
        return
        
class Entries:
    number = 0
    amount = 0
    date = ''
    
    def __init__(self,amt,dte,no):
        self.amount = amt
        self.date = dte

        #+1 here as we increment AFTER we create the entry
        self.number = no
        
    def display_entry(self):
        return '#' + str(self.number) + '  ' + self.date.display_date() + '  ' + str(self.amount)
        
        
class Date:
    day = ''
    month = ''
    year = ''
    
    def __init__(self,d = None ,m = None ,y = None):
            if d == None and m == None and y == None:
                self.day = datetime.datetime.now().date().day
                self.month = datetime.datetime.now().date().month
                self.year = datetime.datetime.now().date().year
            
            self.day= d
            self.month= m
            self.year = y
            
    def display_date(self):
        return str(self.day) + '/' + str(self.month) + '/' + str(self.year) 
            